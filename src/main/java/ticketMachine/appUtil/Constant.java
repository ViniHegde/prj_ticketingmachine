package ticketMachine.appUtil;

import org.javamoney.moneta.Money;

import javax.money.MonetaryAmount;
import java.math.BigDecimal;

/**
 * Constant.java
 *  
 *  Description	: This utility class of the ParkingTicketApp holds all the constant values for the application.
 *  			For example
 *  		    -	the FXML resource path information,
 *  			-   default values for issue ticket screen, etc.
 *
 *  @author 	:   Vinayak Hegde
 *  @version 	:   1.0 November, 2016
 */
public class Constant {
	
    /*
     * Locale codes for the settings, stored in external csv file
     */
	public static final String LOCALE_CODES_FILE_PATH = "/files/Locale_Codes.txt";
    /*
     * Date Formatter for display date and time , again stored in external csv file
     */
    public static final String DATE_FORMATS_FILE_PATH = "/files/Date_Formats.txt";
    /*
     * Resource Path for each FXML used in the application.
     */
    public static final String APP_TITLE = "TicketMachineApp" ;
    public static final String MAIN_LAYOUT_FXML = "/fxml/MainLayout.fxml" ;
    public static final String INIT_SETTINGS_FXML = "/fxml/InitSettings.fxml";
    public static final String ISSUE_TICKET_FXML = "/fxml/IssueTicket.fxml";
    public static final String ADMINISTRATION_FXML = "/fxml/Administration.fxml";
    public static final String LAST_ISSUED_TICKET_FXML = "/fxml/LastIssuedTicket.fxml";
    public static final String SHOW_VEHICLE_DETAILS_FXML = "/fxml/ShowVehicleDetails.fxml";
    public static final String SHOW_EXPIRED_TICKETS_FXML = "/fxml/ExpiredTicketList.fxml";
	
}
