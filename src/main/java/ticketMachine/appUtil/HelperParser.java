package ticketMachine.appUtil;

/** 
 * HelperParser.java
 *  
 *  Description	: This utility class of the parking ticket machine application act as helper class
 *  			It contains the common methods to parse input file, typically CSV 
 * 
 *  @author 	: 	Vinayak Hegde
 *  @version 	:	1.0 November 2016
 */

public class HelperParser {

	private final String DEFAULT_SEPARATOR = "," ;
	/**
     * Default constructor 
     */
	public HelperParser (){
		// Do nothing
	}
	
	/**
     * Parse line from CSV file, for standard format - 
     * NetherLands,nl,NL => {Netherlands,nl,Nl}
     */
    @SuppressWarnings("null")
	public String [] parseCSVText(String lineData){
    	if (lineData == null && lineData.isEmpty()) {		/* if the line data is empty	*/
    		return null ;
    	} else {
    	    return lineData.split(DEFAULT_SEPARATOR);
    	}
    }
}
