package ticketMachine.appUtil;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import org.apache.log4j.Logger;
import org.javamoney.moneta.Money;

import javax.money.MonetaryAmount;
import javax.money.format.MonetaryParseException;
import java.math.BigDecimal;

/** CustomAmountField.java
 *  
 *  Description	: This class belongs to Utility package of the ParkingTicketApp  
 *   which corresponds to Amount text Field for User Input Screen according to Locale
 *
 *  @author 	:   Vinayak Hegde,
 *  @version 	:	1.0 November, 2016
 */
public class CustomAmountField extends TextField {

    private ObjectProperty<MonetaryAmount> amount = new SimpleObjectProperty ();
    private HelperFormatter formatter = new HelperFormatter () ;
    private final static Logger logger = Logger.getLogger(CustomAmountField.class);

    /**
     * Default Constructor for LocalNumberTextField.
     * zero is set as default value.
     */
    public CustomAmountField() {
        this(Money.of(new BigDecimal("0"),"EUR"));
    }

    /**
     * Constructor for LocalNumberTextField with value
     * @param amount value for the amount
     */
    public CustomAmountField(MonetaryAmount amount) {
        super() ;
        setAmount(amount) ;
        initHandlers();
    }

    /**
     * Getter method for amount
     * @return the value of the amount.
     */
    public final MonetaryAmount getAmount() {
        return amount.get();
    }

    /**
     * Setter method for amount
     * @param amount
     */
    public final void setAmount(MonetaryAmount amount) {
        this.amount.set(amount);
    }

    /**
     * Property method for amount
     * @return amount amountProperty is returned.
     */
    public ObjectProperty<MonetaryAmount> amountProperty() {
        return amount;
    }

    /**
     * Initialize event and change handlers for the numeric field.
     */
    private void initHandlers() {

        /*
         * Try to parse when RETURN is hit
         */
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                parseAndFormatInput();
            }
        });

        /*
         * Try to parse when focus is lost
         */
        focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, 
            					Boolean oldValue, Boolean newValue) {
                if (!newValue) {
                    parseAndFormatInput();
                }
            }
        });

        /*
         * Set text in field if MonetaryAmount property is changed from outside.
         */
        amountProperty().addListener(new ChangeListener<MonetaryAmount>() {
            @Override
            public void changed(ObservableValue<? extends MonetaryAmount> obserable,
            					MonetaryAmount oldValue, MonetaryAmount newValue) {
                setText(newValue.toString()) ;
            }
        });
    }

    /**
     * Method to parse the user input to a amount according to the Locale Format
     * if there is no error, the new value is set for the amount.
     */
    private void parseAndFormatInput() {
        try {
            String input = getText();
            if (input == null || input.length() == 0) {
                setText(amount.get().toString()) ;           /* To keep the old value, if cleared or not filled */
                return;
            }
            MonetaryAmount parsedNumber = formatter.getCustomFormat().parse(input);
            if (parsedNumber.getCurrency().equals(amount.get().getCurrency()))
                setAmount(parsedNumber);				                   /* Assign new amount	*/
            selectAll();
        } catch (MonetaryParseException e) {
            System.out.print(e.getMessage());
            logger.error(e.getMessage());
        } finally {
            setText(amount.get().toString()) ;	            /* If parsing fails keep the old amount 	*/
        }
    }
}
