package ticketMachine.appUtil;

import org.javamoney.moneta.format.CurrencyStyle;

import javax.money.MonetaryAmount;
import javax.money.format.AmountFormatQueryBuilder;
import javax.money.format.MonetaryAmountFormat;
import javax.money.format.MonetaryFormats;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Currency;
import java.util.Locale;

/** 
 * HelperFormatter.java
 *  
 *  Description	: This utility class of the parking ticket machine application act as helper class
 *  			It contains the common methods to format either amount value as per locale currency 
 *  			or Date and Time formatting according to Settings.  
 * 
 *  @author 	:   Vinayak Hegde
 *  @version 	:	1.0 November 2016
 */

public class HelperFormatter {

	/**
     * Default constructor 
     */
	public HelperFormatter (){
		// Do nothing
	}

    /**
     * Getter method to custom format
     * @return custom format for Monetary Amount
     */
	public MonetaryAmountFormat getCustomFormat (){
	    return MonetaryFormats.getAmountFormat(Locale.getDefault()) ;
    }
//	/**
//     * Get formatted Paid amount.
//     * @param amount  - Monetary Amount
//     * @return Amount in local currency format
//     */
//    public String getFormattedAmount(MonetaryAmount amount){
//        MonetaryAmountFormat customFormat = MonetaryFormats.getAmountFormat(
//                Locale.getDefault() ) ;
//        return customFormat.format(amount) ;
//    }
    /**
     * Get formatted date and time.
     * @param time  - Date and Time
     * @return Formatted date and time in local format
     */
    public String getFormattedTime(DateTimeFormatter dateTimeFormatter, LocalDateTime time){
        return time.format(dateTimeFormatter ) ;
    }
}
