package ticketMachine.appUtil;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

/** LocalNumberTextField.java
 *  
 *  Description	: This class belongs to Utility package of the ParkingTicketApp  
 *   which corresponds to Numeric text Field for User Input Screen according to Locale
 *
 *  @author 	:   Vinayak Hegde,
 *  @version 	:	1.0 November, 2016
 */
public class LocalNumberTextField extends TextField {

    private final NumberFormat numberFormat;
    private ObjectProperty<BigDecimal> number = new SimpleObjectProperty ();

    /**
     * Default Constructor for LocalNumberTextField.
     * zero is set as default value.
     */
    public LocalNumberTextField() {
        this(BigDecimal.ZERO);
    }

    /**
     * Constructor for LocalNumberTextField with value
     * @param value value for the number
     */
    public LocalNumberTextField(BigDecimal value) {
        this(value, NumberFormat.getInstance(Locale.getDefault()));
        initHandlers();		
    }

    /**
     * Constructor for LocalNumberTextField with value and number format
     * @param value value for the number
     * @param numberFormat number format for the numeric field.
     */
    public LocalNumberTextField(BigDecimal value, NumberFormat numberFormat) {
        super();
        this.numberFormat = numberFormat;
        initHandlers();		
        setNumber(value);
    }

    /**
     * Getter method for number
     * @return the value of the number.
     */
    public final BigDecimal getNumber() {
        return number.get();
    }

    /**
     * Setter method for number
     * @param value 
     */
    public final void setNumber(BigDecimal value) {
        number.set(value);
    }

    /**
     * Property method for number
     * @return number amountProperty is returned.
     */
    public ObjectProperty<BigDecimal> numberProperty() {
        return number;
    }

    /**
     * Initialize event and change handlers for the numeric field.
     */
    private void initHandlers() {

        /*
         * Try to parse when RETURN is hit
         */
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                parseAndFormatInput();
            }
        });

        /*
         * Try to parse when focus is lost
         */
        focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, 
            					Boolean oldValue, Boolean newValue) {
                if (!newValue) {
                    parseAndFormatInput();
                }
            }
        });

        /*
         * Set text in field if BigDecimal property is changed from outside.
         */
        numberProperty().addListener(new ChangeListener<BigDecimal>() {
            @Override
            public void changed(ObservableValue<? extends BigDecimal> obserable, 
            					BigDecimal oldValue, BigDecimal newValue) {
                setText(numberFormat.format(newValue));
            }
        });
    }

    /**
     * Method to parse the user input to a number according to the Number Format
     * if there is no error, the new value is set for the number. 
     */
    private void parseAndFormatInput() {
        try {
            String input = getText();
            if (input == null || input.length() == 0) {
                setText(numberFormat.format(number.get()));	/* To keep the old value, if cleared or not filled */
                return;
            }
            Number parsedNumber = numberFormat.parse(input);
            BigDecimal newValue = new BigDecimal(parsedNumber.toString());
            setNumber(newValue);				/* Assign new number	*/
            selectAll();
        } catch (ParseException ex) {
        	ex.printStackTrace();
        } finally {
        	setText(numberFormat.format(number.get()));	/* If parsing fails keep the old number 	*/
        }
    }
}
