package ticketMachine.model;


import org.javamoney.moneta.Money;

import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.Currency;
import java.util.Locale;

/**
 * ApplicationSettings.java
 *  
 *  Description	: This class belongs to Application Model package of the ParkingTicketApp  
 *   which holds several application settings details
 *      1. Default amount to be shown on the ticket issue screen
 *      2. Country and Language Code
 *      3. Fractional adjust amount and Step adjust amount.
 *      4. Parking rate 
 *      5. Date and Time Format according to Locale
 *
 *  @author 	:   Vinayak Hegde,
 *  @version 	:   1.0	November, 2016
 *  
 */
public class ApplicationSettings {
    
    private MonetaryAmount defaultAmount ;
    private MonetaryAmount adjustInFractions;
    private MonetaryAmount adjustInWholeNbrs ;
    private MonetaryAmount parkingRate ;
    private DateTimeFormatter dateTimeFormatter ;

	/**
	 * Default constructor for ApplicationSettings
     * - Default values are loaded here
	 */
	public ApplicationSettings() {
	    String currencyCode = Currency.getInstance(Locale.getDefault()).getCurrencyCode() ;
        this.defaultAmount = Money.of(new BigDecimal("3.00"), currencyCode ) ;
        this.parkingRate = Money.of(new BigDecimal("0.02"), currencyCode ) ;
        this.adjustInFractions = Money.of(new BigDecimal("0.1"), currencyCode ) ;
        this.adjustInWholeNbrs = Money.of(new BigDecimal("1"), currencyCode ) ;
    }


	/**
	 * Getter method for dateTimeFormatter
	 * @return dateTimeFormatter
	 */
	public DateTimeFormatter getDateTimeFormatter() {
		return dateTimeFormatter;
	}

	/**
	 * Setter method for dateTimeFormatter
	 * @param dateTimeFormatter Date Time Formatter
	 */
	public void setDateTimeFormatter(DateTimeFormatter dateTimeFormatter) {
		this.dateTimeFormatter = dateTimeFormatter;
	}
	
    /**
     * Setter method for default amount
     * @param amount Default Amount
     */
    public void setDefaultAmount(MonetaryAmount amount) {
        this.defaultAmount = amount ;
    }
    
    /**
     * Getter method for defaultAmount
     * @return defaultAmount default amount to be shown for issue ticket screen
     */
    public MonetaryAmount getDefaultAmount( ) {
        return this.defaultAmount;
    }
    
    /**
     * Getter method for parkingRate
     * @return parkingRate
     */
    public MonetaryAmount getParkingRate() {
        return this.parkingRate ;
    }
    
    /**
     * Setter method for parkingRate
     * @param parkingRate Parking rate
     */
    public void setParkingRate(MonetaryAmount parkingRate) {
        this.parkingRate = parkingRate;
    }

    /**
     * Getter method for adjustInFractions
     * @return adjustInFractions
     */
    public MonetaryAmount getAdjustInFractions() {
        return this.adjustInFractions ;
    }
    
    /**
     * Setter method adjustInFractions
     * @param adjustInFractions Adjust amount in fractions
     */
    public void setAdjustInFractions(MonetaryAmount adjustInFractions) {
        this.adjustInFractions = adjustInFractions;
    }
    
    /**
     * Getter method for adjustInWholeNbrs
     * @return adjustInWholeNbrs
     */
    public MonetaryAmount getAdjustInWholeNbrs() {
        return adjustInWholeNbrs;
    }
    
    /**
     * Setter method for adjustInWholeNbrs
     * @param adjustInWholeNbrs Adjust amount as whole number
     */
    public void setAdjustInWholeNbrs(MonetaryAmount adjustInWholeNbrs) {
        this.adjustInWholeNbrs = adjustInWholeNbrs;
    }

    /**
     * Setter Method for Date and Time Format 
     * @param dateTimeFormat Date and Time Format 
     */
    public void setDateTimeFormat(String dateTimeFormat) {
       this.dateTimeFormatter = DateTimeFormatter.ofPattern (dateTimeFormat)  ;
    }
}