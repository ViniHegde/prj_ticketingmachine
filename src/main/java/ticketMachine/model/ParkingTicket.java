package ticketMachine.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.javamoney.moneta.Money;

import javax.money.MonetaryAmount;
import java.time.LocalDateTime;
import java.util.Currency;
import java.util.Locale;

/** 
 * ParkingTicket.java
 *  
 *  Description	: This class belongs to model part of the parking ticket machine 
 *  	which holds details about a ParkingTicket.
 *  
 *  @author 	: Vinayak Hegde
 *  @version 	:	1.0 November 2016
 */

public class ParkingTicket {
    
    private StringProperty licensePlate = new SimpleStringProperty() ;
    private ObjectProperty<LocalDateTime> startTime = new SimpleObjectProperty<>() ;
    private ObjectProperty<LocalDateTime> endTime = new SimpleObjectProperty<>() ;
    private ObjectProperty<MonetaryAmount> paidAmount = new SimpleObjectProperty<> ();
    
    /**
     * default Constructor for the ParkingTicket
     */
    public ParkingTicket() {
        this.licensePlate.set("") ;
        this.startTime.set(LocalDateTime.now()) ;
        this.paidAmount.set(Money.of(0,Currency.getInstance(Locale.getDefault()).getCurrencyCode())) ;
        this.endTime.set(LocalDateTime.now()) ;
    }

    /**
     * Constructor for the Copying ParkingTicket
     * @param curTran - current transaction of the ticket machine
     */
    public ParkingTicket( ParkingTicket curTran) {
            this.licensePlate.set(curTran.getLicensePlate()) ;
            this.paidAmount.set(curTran.getPaidAmount()) ;
            this.startTime.set(curTran.getStartTime()) ;
            this.endTime.set(curTran.getEndTime()) ;
    }

    /**
     * Setter method for licensePlate
     * @param liecencePlate
     */
    public void setLicensePlate (String liecencePlate){
        this.licensePlate.set(liecencePlate) ;
    }
    /**
     * getter method for licensePlate
     * @return license plate number
     */
    public String getLicensePlate (){
        return licensePlate.get() ;
    }
    /**
     * Getter method for start time
     * @return 
     */
    public LocalDateTime getStartTime (){
        return startTime.get() ;
    }
    /**
     * Setter method for start time
     * @param time
     */
    public void setStartTime (LocalDateTime time){
        this.startTime.set(time) ;
    }
    /**
     * Getter method for end time
     * @return 
     */
    public LocalDateTime getEndTime (){
        return endTime.get() ;
    }
    /**
     * Setter method for end time
     * @param time 
     */
    public void setEndTime(LocalDateTime time){
        this.endTime.set(time) ;
    }
    /**
     * Getter method for amount paid
     * @return amount paid
     */
    public MonetaryAmount getPaidAmount(){
        return paidAmount.get() ;
    }
    /**
     * Setter method for amount paid
     * @param amount
     */
    public void setPaidAmount(MonetaryAmount amount){
        this.paidAmount.set(amount) ;
    }
    /**
     * Getter method for liecencePlateProperty
     * @return liecencePlateProperty
     */
    public StringProperty liecencePlateProperty(){
        return this.licensePlate ;
    }
    /**
     * Getter method for startTimeProperty
     * @return startTimeProperty
     */
    public ObjectProperty<LocalDateTime> startTimeProperty(){
        return this.startTime ;
    }
    /**
     * Getter method for endTimeProperty
     * @return endTimeProperty
     */
    public ObjectProperty<LocalDateTime> endTimeProperty(){
        return this.endTime ;
    }
    /**
     * Getter method for paidAmountProperty
     * @return paidAmountProperty
     */
    public ObjectProperty<MonetaryAmount> paidAmountProperty(){
        return this.paidAmount ;
    }

}

