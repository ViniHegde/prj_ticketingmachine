package ticketMachine;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import ticketMachine.appUtil.Constant;
import ticketMachine.controller.MainLayoutController;

/** 
 * TicketMachine.java
 *  
 *  Description	: This is the main class of the parking ticket machine application. 
 *     			this class is on top of the hierarchy and the application starts from here.
 * 
 *  @author 	:   Vinayak Hegde
 *  @version 	:   1.0	November 2016
 *  
 *  Modification History -
 */
public class TicketMachine extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {
        /*
         * Load Main layout fxml file and pass on the reference to MainLayoutController class 
         * Main Layout is a BorderPane where the Center Pane  is used for displaying different scenes.
         */
        FXMLLoader loader = new FXMLLoader(getClass().getResource(Constant.MAIN_LAYOUT_FXML));
        BorderPane mainLayout = loader.load();
        MainLayoutController  mainLayoutController = loader.getController() ;
        mainLayoutController.setTicketMachineMain(primaryStage, mainLayout);
        
        Scene scene = new Scene(mainLayout); 				/* Initial scene set to Main Layout */      			
        primaryStage.setTitle(Constant.APP_TITLE);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Main method to launch application
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // launch the application
        launch(args);
    }
}
