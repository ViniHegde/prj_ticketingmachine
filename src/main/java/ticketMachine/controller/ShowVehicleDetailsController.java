package ticketMachine.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ticketMachine.appUtil.HelperFormatter;
import ticketMachine.model.ParkingTicket;

/** 
 * ShowVehicleDetailsController.java
 *  
 *  Description	: This class belongs to controller part of the parking ticket machine 
 *      It is the Controller for the ShowVehicleDetails.fxml. 
 *      When user search by license number, the details are displayed with pop-up window.
 * 
 *  @author 	: Vinayak Hegde
 *  @version 	:	1.0 	November 2016
 */
public class ShowVehicleDetailsController implements Initializable {

    private MainLayoutController ticketMachineMain;				/* Reference to the main application.	*/
    private HelperFormatter formatter ;
    
    private Stage dialogStage;
    
    @FXML private TextField licencePlateNbrField;
    @FXML private TextField amountField;
    @FXML private TextField endTimeField;
    @FXML private TextField startTimeField ;
    
    /**
     * Default constructor
     */
    public ShowVehicleDetailsController () {
    	formatter = new HelperFormatter () ;
    }
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
           //
    }
    
    /**
     * Method to set reference to Main Controller. 
     * Called only once to make reference available inside child controllers
     * @param ticketMachineMain the Main Controller of TicketMachine
     */
    public void setMainLayoutController (MainLayoutController ticketMachineMain ) {
        this.ticketMachineMain = ticketMachineMain ;
    }

    /**
     * Clears and sets all the default values to show the user with issue ticket screen.
     * Default amount is set based on the ApplicationSettings 
     * @param searchTran
     */
    public void setDisplayFields(ParkingTicket searchTran) {
        licencePlateNbrField.setText(searchTran.getLicensePlate());
        amountField.setText(searchTran.getPaidAmount()
            .toString() );
        endTimeField.setText(
        		formatter.getFormattedTime(ticketMachineMain.getApplicationSettings().getDateTimeFormatter(), 
        				searchTran.getEndTime() ) );
        startTimeField.setText(
        		formatter.getFormattedTime(ticketMachineMain.getApplicationSettings().getDateTimeFormatter(),
                        searchTran.getStartTime() ) );
    }

     /**
     * Called when the user clicks on the search button
     */
    @FXML
    private void handleOKButton () {
        dialogStage.close();
    }

    /**
     * Sets the stage of this dialog.
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
}
