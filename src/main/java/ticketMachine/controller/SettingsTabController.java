package ticketMachine.controller;

import java.math.BigDecimal;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import org.javamoney.moneta.Money;
import ticketMachine.appUtil.CustomAmountField;
import ticketMachine.appUtil.LocalNumberTextField;
import ticketMachine.model.ApplicationSettings;

import javax.money.MonetaryAmount;

/** 
 * SettingsTabController.java
 *  
 *  Description	: This class belongs to controller part of the parking ticket machine application 
 *      It is the Controller for the ApplicationSettings.fxml. 
 *      Upon user's input the ApplicationSettings will be applied
 *      User can update/change following settings -
 *      	1.Default Amount 
 *      	2.Adjust amount
 *      	3.Parking Rate
 * 
 *  @author 	:   Vinayak Hegde
 *  @version 	:	1.0 November 2016
 */
public class SettingsTabController implements Initializable {

    private MainLayoutController ticketMachineMain;
    
    /**
     * ApplicationSettings to store and modify the settings
     */
    private ApplicationSettings settings ;

    @FXML private CustomAmountField parkingRate;
    @FXML private CustomAmountField defaultAmount;
    @FXML private CustomAmountField stepAdjust;
//    @FXML private Label curSymbolDefaultAmount ;
//    @FXML private Label curSymbolParkingRate ;

    /**
     * Default constructor
     */
    public SettingsTabController() {
        // Do nothing
    }
    
    /**
     * Initializes the controller class.
     * @param url URL
     * @param rb Resource Bundle
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Do nothing
    }    

    /**
     * Method to set reference to Main Controller. 
     * Called only once to make reference available inside child controllers
     * @param ticketMachineMain the Main Controller of TicketMachine
     */
    public void setMainLayoutController(MainLayoutController ticketMachineMain) {
        this.ticketMachineMain = ticketMachineMain ;
        this.settings = ticketMachineMain.getApplicationSettings() ;
        setDisplayFields();
    }

    /**
     * Clears and sets all the default values to show the user with issue ticket screen.
     * Default amount is set based on the ApplicationSettings 
     */
    private void setDisplayFields() {
        parkingRate.setAmount(settings.getParkingRate());
        defaultAmount.setAmount(settings.getDefaultAmount());
        stepAdjust.setAmount(settings.getAdjustInWholeNbrs());
//        curSymbolDefaultAmount.setText(settings.getDefaultAmount().getCurrency().getCurrencyCode()) ;
//        curSymbolParkingRate.setText(settings.getParkingRate().getCurrency().toString()) ;
    }
    
    /**
     * Called when the user clicks on the Apply button
     */
    @FXML
    private void applyChanges() {
        if (isValidInputs()) {
            settings.setParkingRate(parkingRate.getAmount()) ;
            settings.setDefaultAmount(defaultAmount.getAmount());
            settings.setAdjustInWholeNbrs(stepAdjust.getAmount() );
            ticketMachineMain.showConfirmationMessage("Success",
                    "Settings Changed.",
                    "The new changes have been applied !");
        }
    }

    /**
     * validation method for user inputs on settings tab.
     * @return true if all user inputs are valid
     */
    private boolean isValidInputs() {
        MonetaryAmount oneUnit = Money.of(new BigDecimal("1"), settings.getDefaultAmount().getCurrency()) ;
        if (defaultAmount.getAmount().isLessThanOrEqualTo(oneUnit)){
            ticketMachineMain.showErrorMessage("Invalid Amount",
                    "Invalid Default Amount",
                    "Please set the correct amount as Default Amount.");
            return false;
        }else if (parkingRate.getAmount().isNegativeOrZero() ){
            ticketMachineMain.showErrorMessage("Invalid Amount",
                    "Invalid Parking Rate",
                    "Please set the correct amount for Parking Rate.");
            return false;
        }else if (stepAdjust.getAmount().isLessThan(oneUnit))  {
            ticketMachineMain.showErrorMessage("Invalid Amount",
                    "Invalid Step Adjust",
                    "Please set the correct whole number for Step Adjust.");
            return false ;
        } else {
            return true ;
        }
    }
}