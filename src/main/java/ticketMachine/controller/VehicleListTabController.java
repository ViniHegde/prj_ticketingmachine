package ticketMachine.controller;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ticketMachine.appUtil.Constant;
import ticketMachine.appUtil.HelperFormatter;
import ticketMachine.model.ParkingTicket;

import javax.money.MonetaryAmount;

/** 
 * VehicleListTabController.java
 *  
 *  Description	: This class belongs to controller part of the parking ticket machine 
 *      It is the Controller for the VehicleListTab.fxml. 
 *      Table view of issued tickets will be shown to user.
 * 
 *  @author 	: Vinayak Hegde
 *  @version 	:	1.0 	November 2016
 */
public class VehicleListTabController implements Initializable {

    private MainLayoutController ticketMachineMain ;
    private HelperFormatter formatter ;
    
    private ParkingTicket searchTran ;							
    private ObservableList<ParkingTicket> expiredTickets ;
    
    @FXML private TextField searchByLicenceNumber;
    @FXML private TableView<ParkingTicket> vehiclesTable;
    @FXML private TableColumn<ParkingTicket, String> liecencePlateColumn;
    @FXML private TableColumn<ParkingTicket, LocalDateTime> startTimeColumn;
    @FXML private TableColumn<ParkingTicket, LocalDateTime> endTimeColumn;
    @FXML private TableColumn<ParkingTicket, MonetaryAmount> amountColumn;
    
    /**
     * Default constructor
     */
    public VehicleListTabController () {
    	formatter = new HelperFormatter () ;
    	expiredTickets = FXCollections.observableArrayList();
    }
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        liecencePlateColumn.setCellValueFactory(cellData -> cellData.getValue().liecencePlateProperty());
        startTimeColumn.setCellValueFactory((TableColumn.CellDataFeatures<ParkingTicket, LocalDateTime> cellData) -> cellData.getValue().startTimeProperty());
        endTimeColumn.setCellValueFactory((TableColumn.CellDataFeatures<ParkingTicket, LocalDateTime> cellData) -> cellData.getValue().endTimeProperty());
        amountColumn.setCellValueFactory((TableColumn.CellDataFeatures<ParkingTicket, MonetaryAmount> cellData) -> cellData.getValue().paidAmountProperty());
    }    

    /**
     * Set Main Layout Controller ()
     * Called only once at the beginning immediately after loading
     * @param ticketMachineMain 
     */
    void setMainLayoutController(MainLayoutController ticketMachineMain) {
        this.ticketMachineMain = ticketMachineMain ;
        vehiclesTable.setItems(ticketMachineMain.getVehicleList()) ;
//        loadFxmlShowVehicleDetails() ;
    }
    
    /**
     * Update/Refresh Table View Data.
     */
    @FXML
    public void refreshTableCellData() {
        
    	/*
         *  Custom rendering in currency format for amount column
         */
    	amountColumn.setCellFactory((TableColumn<ParkingTicket, MonetaryAmount> column) -> new TableCell<ParkingTicket, MonetaryAmount>() {
            @Override
            protected void updateItem(MonetaryAmount item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setText(null);
                } else {
                    // Format date and time as per Settings Format .
                    setText(item.toString() );
                }
            }
        });
       
        /*
         *  Custom rendering for Date and Time Fields for start time column
         */
        startTimeColumn.setCellFactory((TableColumn<ParkingTicket, LocalDateTime> column) -> new TableCell<ParkingTicket, LocalDateTime>() {
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setText(null);
                } else {
                    // Format date and time as per Settings Format .
                    setText(formatter.getFormattedTime( 		
                    				ticketMachineMain.getApplicationSettings().getDateTimeFormatter(),
                    				item ) );
                }
            }
        });
       
        /*
         *  Custom rendering for Date and Time Fields for end time column
         *  change the background for expired tickets
         */
        endTimeColumn.setCellFactory((TableColumn<ParkingTicket, LocalDateTime> column) -> {
            return new TableCell<ParkingTicket, LocalDateTime>() {
                @Override
                protected void updateItem(LocalDateTime item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        // Format date and time as per Settings Format .
                        setText(formatter.getFormattedTime(
                                ticketMachineMain.getApplicationSettings().getDateTimeFormatter(),
                                item));

                        // Set Style for Timed Out vehicles.
                        TableRow<ParkingTicket> currentRow = getTableRow();
                        if (item.isBefore(LocalDateTime.now())) {
                            setTextFill(Color.CHOCOLATE);
                            currentRow.setStyle("-fx-background-color: yellow");
                        } else {
                            setTextFill(Color.BLACK);
                            currentRow.setStyle("");
                        }
                    }
                }
            };
        });
    }    
    
    /**
     * Called when the user clicks on the search button
     */
    @FXML
    private void handleSearchButton() {
        String licenceNbr = searchByLicenceNumber.getText() ;
        if (isLicenceNbrFound(licenceNbr) ) {
	    	/*
	    	 * Load the Dialog window for Search Item
	    	 */
	        try{
	        	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(Constant.SHOW_VEHICLE_DETAILS_FXML));
	        	SplitPane showVehicleDetailsPane = fxmlLoader.load();
	        	ShowVehicleDetailsController showVehiclesDetailsController = fxmlLoader.getController() ;
	            showVehiclesDetailsController.setMainLayoutController(ticketMachineMain);
	   
	        	Stage dialogStage = new Stage();
	        	dialogStage.setTitle("Search Result");
	    		dialogStage.initModality(Modality.WINDOW_MODAL);
	    		dialogStage.initOwner(ticketMachineMain.getPrimaryStage());
	    		Scene scene = new Scene(showVehicleDetailsPane);
	    		dialogStage.setScene(scene);
	    		showVehiclesDetailsController.setDialogStage(dialogStage);
	            showVehiclesDetailsController.setDisplayFields(searchTran);
	            dialogStage.showAndWait();
	        }catch(Exception e){
	            System.out.println(e.getMessage());
	        }
	        /*
	         * Clear display field after successful display of details
	         */
	        searchByLicenceNumber.clear();
        } 
    }

    /**
     * validation method for user inputs on search field.
     * @param licenceNbr
     * @return true if the license number is found within the list.
     */
    private boolean isLicenceNbrFound(String licenceNbr) {
    	for (ParkingTicket ticket : ticketMachineMain.getVehicleList()) {
            if (licenceNbr.equals(ticket.getLicensePlate())) {
                searchTran = new ParkingTicket(ticket) ;
                return true ;
            }
        }
        return false;
    }

    /**
     * Called when the user clicks on the filter by expire button
     */
    @FXML
    private void handleFilterByExpirationButton () {
    	expiredTickets.clear();											/* Clear Expiry List	*/
    	/*
    	 * For each ticket, check if the end time has lapsed by now.
    	 */
    	for (ParkingTicket ticket : ticketMachineMain.getVehicleList()) {
    		if (ticket.getEndTime().isBefore(LocalDateTime.now())) {
    			addToExpiryList(ticket) ;
            }
        }
    	/*
    	 * Load the Dialog window for Expired Ticket List
    	 */
        try{
        	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(Constant.SHOW_EXPIRED_TICKETS_FXML));
        	AnchorPane expiredTicketListPane = fxmlLoader.load();
        	ExpiredTicketListController expiredTicketListController = fxmlLoader.getController() ;
            expiredTicketListController.setMainLayoutController(ticketMachineMain, expiredTickets );
   
        	Stage dialogStage = new Stage();
        	dialogStage.setTitle("Search Result");
    		dialogStage.initModality(Modality.WINDOW_MODAL);
    		dialogStage.initOwner(ticketMachineMain.getPrimaryStage());
    		Scene scene = new Scene(expiredTicketListPane) ;
    		dialogStage.setScene(scene);
    		expiredTicketListController.setDialogStage(dialogStage);
            expiredTicketListController.refreshTableCellData();
            dialogStage.showAndWait();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * add a transaction to Expired Ticket List
     * @param parkingTicket object with complete parking ticket details.
     */
     public void addToExpiryList(ParkingTicket parkingTicket) {
    	 expiredTickets.add(parkingTicket) ;
     }

}
