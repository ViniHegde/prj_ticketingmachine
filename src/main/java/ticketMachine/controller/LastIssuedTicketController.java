package ticketMachine.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import ticketMachine.appUtil.CustomAmountField;
import ticketMachine.appUtil.HelperFormatter;
import ticketMachine.model.ParkingTicket;

/** IssueTicketController.java
 *  
 *  Description	: This class belongs to view part of the parking ticket machine 
 *      which displays the details for the last issued ticket.
 * 
 *  @author 	: Vinayak Hegde
 *  @version 	:	1.0 	November 2016
 */
public class LastIssuedTicketController 
    implements Initializable {

    private IssueTicketController issueTicketController;				/* Reference to the main application.	*/
    private HelperFormatter formatter ;
    
    @FXML private TextField licencePlateNbrField;
    @FXML private CustomAmountField amountField;
    @FXML private TextField endTimeField;
    @FXML private TextField startTimeField ;

    /**
     * Default constructor - called before initialize() method
     */
    public LastIssuedTicketController () {
    	formatter = new HelperFormatter () ;
    }
    
    /**
     * Method to transfer control from Issue Ticket Controller to Show Ticket Controller 
     * @param issueTicketController
     */
	protected void setMainLayoutController(IssueTicketController issueTicketController) {
		this.issueTicketController = issueTicketController  ;
	}
	
    /**
     * Show Last Issued Ticket. Called from Issue Ticket Screen Pane
     * @param curTran
     */
    public void showIssuedTicket (ParkingTicket curTran) {
        setDisplayFields(curTran);			// Set default display details.
    }

    /**
     * Clears and sets all the default values to show the user with issue ticket screen.
     * Default amount is set based on the ApplicationSettings 
     */
    private void setDisplayFields(ParkingTicket curTran) {
        licencePlateNbrField.setText(curTran.getLicensePlate());
        amountField.setText(curTran.getPaidAmount()
                .toString() );
        endTimeField.setText(
        		formatter.getFormattedTime(
        				issueTicketController.getApplicationSettings().getDateTimeFormatter(), 
        				curTran.getEndTime()) );
        startTimeField.setText(
        		formatter.getFormattedTime(
        				issueTicketController.getApplicationSettings().getDateTimeFormatter(), 
        				curTran.getStartTime() ) );
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    	// Do nothing
    }
}
