package ticketMachine.controller;

import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import ticketMachine.appUtil.Constant;
import ticketMachine.appUtil.CustomAmountField;
import ticketMachine.appUtil.HelperFormatter;
import ticketMachine.model.ApplicationSettings;
import ticketMachine.model.ParkingTicket;

import javax.money.MonetaryAmount;

/** 
 * IssueTicketController.java
 *  
 *  Description	: This class belongs to controller part of the parking ticket machine 
 *      Issue Ticket screen is handled here. 
 *      Upon valid user inputs, the Ticket details are added to VehicleList of Main Controller class.
 * 
 *  @author 	: Vinayak Hegde 
 *  @version 	:	1.0 	November 2016
 */
public class IssueTicketController 
    implements Initializable {
	
    private MainLayoutController ticketMachineMain;		/* Reference to the main controller.	*/
    private ApplicationSettings settings ;
    private HelperFormatter formatter ;
    private ParkingTicket curTran ;						/* Current Transaction	*/ 
    
    private SplitPane lastIssuedTicket ;
    private LastIssuedTicketController lastIssuedTicketcontroller ;
    
    @FXML private TextField licencePlateNbrField;
    @FXML private CustomAmountField amountField;
    @FXML private TextField endTimeField;

    /**
     * Default constructor 
     */
    public IssueTicketController () {
    	curTran = new ParkingTicket () ;
    	formatter = new HelperFormatter () ;
    }
    
    /**
     * Getter method for ApplicationSettings
     * @return settings
     */
    public ApplicationSettings getApplicationSettings ()   {
        return this.settings ;
    }
    
    /**
     * Method to set reference to Main Controller. 
     * Called only once to make reference available inside child controllers
     * @param ticketMachineMain the Main Controller of TicketMachine
     */
    public void setMainLayoutController (MainLayoutController ticketMachineMain) {
        this.ticketMachineMain = ticketMachineMain ;
        this.settings = ticketMachineMain.getApplicationSettings() ;
        loadFxmlShowTicket() ;									/* Load Show Ticket Details FXML	*/
    }

    /**
     * sets all the display fields on the issue ticket screen.
     * Default Values are set based on the ApplicationSettings 
     */
    private void setDisplayFields() {
        licencePlateNbrField.setText(curTran.getLicensePlate());
        setAmountAndTimeDisplayFields();
    }
    
    /**
     * Sets the Amount and End time values of the display fields.
     */
    private void setAmountAndTimeDisplayFields() {
        amountField.setText(curTran.getPaidAmount()
                .toString() );
        endTimeField.setText(
        		formatter.getFormattedTime(settings.getDateTimeFormatter(), curTran.getEndTime()) );
    }
    
    /**
    * Update End time according to total paid amount.
    */
    private void updateEndTime(){
        LocalDateTime increasedTime = curTran.getStartTime() ;
        increasedTime = increasedTime.plusMinutes(
                calculateTimePerPaidAmount(curTran.getPaidAmount() )
                    ) ;
        curTran.setEndTime(increasedTime)	;
    }
    
    /**
     * To calculate number of minutes to increase/decrease based upon the amount.
     * @param amount in whole numbers
     * @return minutes proportional to amount.
     */
    private long calculateTimePerPaidAmount(MonetaryAmount amount){
    	/*
    	 *  Handle divide by zero error here
    	 */
    	try {
    		return (long) ( amount.getNumber().doubleValue() /
                                settings.getParkingRate().getNumber().doubleValue());
    	} catch (ArithmeticException ae){
    		return 0 ;
    	}
    }

    /**
     * Method to start new transaction
     */
    protected void startNewTransaction() {
        curTran.setLicensePlate("");
        curTran.setStartTime(LocalDateTime.now());
        curTran.setPaidAmount(settings.getDefaultAmount());
        updateEndTime() ;
        setDisplayFields();
    }
    
    /**
     * Called when the user clicks on the administration button
     * Set the scene as Administration 
     */
    @FXML
    private void handleAdministrationButton() {
    	ticketMachineMain.setSceneToAdministration() ;
    }
    
    /**
     * Called when the user clicks on the decrease by fraction button
     */
    @FXML
    private void handleDecreaseByFractions() {
        removeMoney (settings.getAdjustInFractions()) ;
        updateEndTime() ;
        setAmountAndTimeDisplayFields();
    }
    
    /**
     * Called when the user clicks on the decrease by fraction button
     */
    @FXML
    private void handleDecreaseByWholeNbrs() {
        removeMoney (settings.getAdjustInWholeNbrs()) ;
        updateEndTime() ;
        setAmountAndTimeDisplayFields();
    }

    /**
    * RemoveTime/removeMoney method - for corresponding user action.
    * @param adjustAmount - total paid amount
    */
    private void removeMoney(MonetaryAmount adjustAmount) {
        if(curTran.getPaidAmount().subtract(adjustAmount).isNegativeOrZero())
        {
            ticketMachineMain.showErrorMessage("Amount Adjustment",
                    "Amount Reduction",
                    "You have already reached the minimum amount!");
        } else {
            curTran.setPaidAmount(curTran.getPaidAmount().subtract(adjustAmount)) ;
        }
    }

    /**
     * Called when the user clicks on the decrease by fraction button
     */
    @FXML
    private void handleIncreaseByFractions() {
        addMoney (settings.getAdjustInFractions()) ;
        updateEndTime() ;
        setAmountAndTimeDisplayFields();
    }
    
    /**
     * Called when the user clicks on the decrease by fraction button
     */
    @FXML
    private void handleIncreaseByWholeNbrs() {
        addMoney (settings.getAdjustInWholeNbrs()) ;
        updateEndTime() ;
        setAmountAndTimeDisplayFields();
    }
    
    /**
     * Called when the user clicks on the done button
     */
    @FXML
    private void handleDoneButton() {
        String licencePlateNbr = licencePlateNbrField.getText();

        if (isValidLicencePlate(licencePlateNbr)) {
            curTran.setLicensePlate(licencePlateNbr);
            ticketMachineMain.finishTransaction(new ParkingTicket(curTran)) ;
            ticketMachineMain.setMainLayoutCenter(lastIssuedTicket) ;
            lastIssuedTicketcontroller.showIssuedTicket(curTran);
            
            /*
             *  Set Timer for the Show Ticket Panel	
             */
            Timeline timeline = new Timeline(new KeyFrame(
            Duration.millis(3600),
            ae -> ticketMachineMain.setSceneToIssueTicket()));
            timeline.play();
        }
    }
    
    /**
     * Method to check if the user inputs are valid.
     * @param licencePlateField Licence Plate Number
     * @return true if all user inputs are valid.
     */
    private boolean isValidLicencePlate(String licencePlateField) {
        boolean isValid = false;
        try {
        	/*
        	 * retrieve the values from the text field, and validate for NNNNAA format
        	 */
            if(Pattern.matches("[0-9]{4}[A-Z]{2}", licencePlateField))
            {
                    isValid = true ;
            } else
            {
                ticketMachineMain.showErrorMessage( "ParkingTicket Number",
                                  "Invalid ParkingTicket Number Format ",
                                  "An error has occured due to incorrect \"ParkingTicket Number\" text field data.\nVehicle Number should be NNNNAA format where NNNN is digits and AA is alphabets.");
            }
        /*
         * NumberFormatException would usually be thrown if the text fields
         * contain invalid data, for example a price field containing letters.
         */
        } catch (NumberFormatException exp) {
            ticketMachineMain.showErrorMessage("ParkingTicket Number",
                            "Licence Number Not Valid",
                             "An unknown error has occured. Please ensure your fields meet the following requirements:"
                           + "\nVehicle Number should be NNNNAA format where NNNN is digits and AA is alphabets.");
        }
        return isValid ;
}
    
    /**
     * AddTime/addMoney method - for corresponding user action.
     * @param adjustAmount - total Paid amount
     */
    private void addMoney(MonetaryAmount adjustAmount) {
       curTran.setPaidAmount(curTran.getPaidAmount()
               .add(adjustAmount)   );
    }

    
    /**
     * Set the scene for parking ticket
     * Is the default scene for the main layout and being called from Parking Ticket Button on main screen
     */
    public void loadFxmlShowTicket() {
            try{
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(Constant.LAST_ISSUED_TICKET_FXML));
                lastIssuedTicket = fxmlLoader.load();
                lastIssuedTicketcontroller = fxmlLoader.getController() ;
                lastIssuedTicketcontroller.setMainLayoutController(this) ;
            }catch(Exception e){
                System.out.println(e.getMessage());
            }
    }
    
    /**
     * Default initialize method for FXML
     * @param arg0
     * @param arg1
     */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
	}    
}
