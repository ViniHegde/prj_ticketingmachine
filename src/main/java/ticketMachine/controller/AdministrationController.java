package ticketMachine.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/** AdministrationController.java
 *  
 *  Description	: This class belongs controller part of the ParkingTicketApp  
 *   which corresponds to Administration window panel
 *
 *  @author 	: Vinayak Hegde, November, 2016
 *  @version 	:	1.0 
 */
public class AdministrationController implements Initializable {

    private MainLayoutController ticketMachineMain;						/* Reference to the main controller.	*/

    @FXML private SettingsTabController settingsTabController ;
    @FXML private VehicleListTabController vehicleListTabController ;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    /**
     * Called when the user clicks on the parking ticket button
     * User will be navigated to Issue Ticket Screen
     */
    @FXML
    private void handleGoToIssueTicketButton() {
        // Set the scene as Parking Ticket Issue 
        ticketMachineMain.setSceneToIssueTicket() ;
    }
    
    /**
     * Method to set reference to Main Controller. 
     * Called only once to make reference available inside child controllers
     * @param ticketMachineMain the Main Controller of TicketMachine
     */
    protected void setMainLayoutController(MainLayoutController ticketMachineMain) {
        this.ticketMachineMain = ticketMachineMain ;
        settingsTabController.setMainLayoutController(ticketMachineMain) ;
        vehicleListTabController.setMainLayoutController(ticketMachineMain) ;
    }
    
    /**
     * Refresh the Table View of ParkingTicket List tab.
     */
	protected void refreshTable() {
		vehicleListTabController.refreshTableCellData() ;
	}
}
