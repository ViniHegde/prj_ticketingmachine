package ticketMachine.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.PasswordField;
import org.apache.log4j.Logger;
import ticketMachine.appUtil.Constant;
import ticketMachine.appUtil.HelperParser;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.TreeMap;

/** 
 * InitSettingsController.java
 *  
 *  Description	: This class belongs to controller part of the parking ticket machine 
 *      It is the Controller for the InitSettings.fxml. 
 *      Locale Code details are loaded from Locale_Codes.csv and Date time formats from Date_Time_Formats.csv
 *      Upon user's choice the default locale codes are set and values are set in ApplicationSettings.
 *      User can also choose following settings -
 *      	1.Country 
 *      	2.Date and Time format
 *      	3.Admin Code
 * 
 *  @author 	:   Vinayak Hegde
 *  @version 	:	1.0 November 2016
 */
public class InitSettingsController 
        implements Initializable{
	
    private MainLayoutController ticketMachineMain;		/* Reference to the main controller.	*/
    final static Logger logger = Logger.getLogger(InitSettingsController.class);
    
    /* 
     * Instance variables corresponding to FXML	
     */
    private ObservableList<String> countryList ;
    private TreeMap<String, String[]> localeCodesMap ;
    private ObservableList<String> dateTimeFormatterList ;
    
    @FXML private ChoiceBox<String> choiceBoxCountry ;
    @FXML private ChoiceBox<String> choiceBoxDateTimeFormatter;
    @FXML private PasswordField adminPassCode ;
    
    /**
     * Default constructor - for Main Layout controller
     */
    public InitSettingsController () {
        localeCodesMap = new TreeMap<>();
        countryList = FXCollections.observableArrayList();
        dateTimeFormatterList = FXCollections.observableArrayList();
    }
    
    /**
     * Method to set reference to Main Controller. 
     * Called only once to make reference available inside child controllers
     * @param ticketMachineMain the Main Controller of TicketMachine
     */
    void setMainLayoutController(MainLayoutController ticketMachineMain) {
    	this.ticketMachineMain = ticketMachineMain ;
        loadLocaleCodes() ;							/* Load Country Codes 	*/
        loadDateTimeFormatterCodes() ;				/* Load Date and Time Format	 */
        choiceBoxCountry.getSelectionModel().selectFirst();
        choiceBoxDateTimeFormatter.getSelectionModel().selectFirst();
        adminPassCode.clear();
    }
    
    /**
     * Load Locale Codes from Locale_Codes.txt file.
     * by default Locale Codes for Netherlands is chosen.
     */
    private void loadLocaleCodes() {
        HelperParser parser = new HelperParser () ;
    	try (Scanner dataInScanner = new Scanner(new File(
                this.getClass().getResource(Constant.LOCALE_CODES_FILE_PATH).toURI())))
        {
            while (dataInScanner.hasNext()) {
            	/*
            	 * Read the line data into an Array. Each line from the external file has to be in the CSV format
            	 * Example data line - Netherlands,NL,nl
            	 */
            	String[] countryAndCodes = parser.parseCSVText(
            								dataInScanner.nextLine() );
            	if ((countryAndCodes != null) &(countryAndCodes.length==3)) {
                    countryList.add(countryAndCodes[0]) ;
                    String[] localeCodes = {countryAndCodes[1],countryAndCodes[2]} ;
                    localeCodesMap.put(countryAndCodes[0], localeCodes) ;
                }// if condition
            }// while loop
            dataInScanner.close();
        } catch (IOException | URISyntaxException e) {
            logger.error(e.getMessage());
        } finally {
        	/*
        	 * If there is no data in the external file, then default to Netherlands
        	 */
	    	if (countryList.isEmpty()) {
	            countryList.add("Netherlands") ;
	            String[] localeCodes = {"nl","NL"} ;
	            localeCodesMap.put("Netherlands", localeCodes) ;
	        }
        }
        choiceBoxCountry.setItems(countryList);
    }
    
    /**
     * Load Date and Time Formats from Date_Time_Formats.csv file.
     * by default Locale Codes for Netherlands is chosen.
     */
    private void loadDateTimeFormatterCodes() {
        HelperParser parser = new HelperParser () ;
    	try (Scanner dataInScanner = new Scanner(new File(
    	       this.getClass().getResource(Constant.DATE_FORMATS_FILE_PATH).toURI())))
        {
            while (dataInScanner.hasNext()) {
            	/*
            	 * Read the line data into an Array. 
            	 * Each line from the external file has to be in the CSV format 
            	 * Example data line - "dd/MM/yyyy - HH:mm:ss"
            	 */
            	String[] dateTimeFormat = parser.parseCSVText(dataInScanner.nextLine() );
            	if (dateTimeFormat.length > 0) {
            		dateTimeFormatterList.add(dateTimeFormat[0]) ;
            	}
            }	// while loop
            dataInScanner.close();
        } catch (IOException | URISyntaxException e) {
            logger.error(e.getMessage());
        } finally {
        	/*
        	 * If there is no data in the external file, then default to DD/MM/YYYY - HH:MM:SS format
        	 */
	    	if (dateTimeFormatterList.isEmpty()) {
	    		dateTimeFormatterList.add("dd/MM/yyyy - HH:mm:ss") ;
	        }
        }
        choiceBoxDateTimeFormatter.setItems(dateTimeFormatterList);
    }
    
    /**
     * Called when the user clicks on confirm button from first screen
     */
    @FXML
    private void handleConfirmButton() {
    	ticketMachineMain.createSettings(                               // Set Locale Code and Date Time formatter
                localeCodesMap.get(choiceBoxCountry.getValue()),
                choiceBoxDateTimeFormatter.getValue() );
    	ticketMachineMain.loadFxmlIssueTicket() ;			            // Load Issue Parking Ticket Pane
    	ticketMachineMain.loadFxmlAdministration () ;		            // Load Administration
    	ticketMachineMain.setSceneToIssueTicket() ;			            // Set the scene as Parking Ticket Issue
    }

    /**
     * Default initialize method for FXML
     */
    @Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
	}
}
