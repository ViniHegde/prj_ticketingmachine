package ticketMachine.controller;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import ticketMachine.appUtil.HelperFormatter;
import ticketMachine.model.ParkingTicket;

import javax.money.MonetaryAmount;

/** 
 * ExpiredTicketListController.java
 *  
 *  Description	: This class belongs to controller part of the parking ticket machine 
 *      It is the Controller for the VehicleListTab.fxml. 
 *      Table view of issued tickets will be shown to user.
 * 
 *  @author 	: Vinayak Hegde
 *  @version 	:	1.0 	November 2016
 */
public class ExpiredTicketListController implements Initializable {

    private MainLayoutController ticketMachineMain ;
    private HelperFormatter formatter ;
    private Stage dialogStage;
    
//    private ObservableList<ParkingTicket> expiredTickets ;

    @FXML private TableView<ParkingTicket> expiredTicketsTable;
    @FXML private TableColumn<ParkingTicket, String> liecencePlateColumn;
    @FXML private TableColumn<ParkingTicket, LocalDateTime> startTimeColumn;
    @FXML private TableColumn<ParkingTicket, LocalDateTime> endTimeColumn;
    @FXML private TableColumn<ParkingTicket, MonetaryAmount> amountColumn;
    
    /**
     * Default constructor
     */
    public ExpiredTicketListController () {
    	formatter = new HelperFormatter () ;
    }
    
    /**
     * Set Main Layout Controller ()
     * Called only once at the beginning immediately after loading
     * @param ticketMachineMain Reference to Ticket Machine Main
     */
    void setMainLayoutController(MainLayoutController ticketMachineMain, 
    		ObservableList<ParkingTicket> expiredTickets) {
        this.ticketMachineMain = ticketMachineMain ;
        expiredTicketsTable.setItems(expiredTickets) ;
    }
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        liecencePlateColumn.setCellValueFactory(cellData -> cellData.getValue().liecencePlateProperty());
        startTimeColumn.setCellValueFactory((TableColumn.CellDataFeatures<ParkingTicket, LocalDateTime> cellData) -> cellData.getValue().startTimeProperty());
        endTimeColumn.setCellValueFactory((TableColumn.CellDataFeatures<ParkingTicket, LocalDateTime> cellData) -> cellData.getValue().endTimeProperty());
        amountColumn.setCellValueFactory((TableColumn.CellDataFeatures<ParkingTicket, MonetaryAmount> cellData) -> cellData.getValue().paidAmountProperty());
    }    

    /**
     * Update Table View Data.
     */
    public void refreshTableCellData() {
        
    	/*
         *  Custom rendering in currency format for amount column
         */
    	amountColumn.setCellFactory((TableColumn<ParkingTicket, MonetaryAmount> column) -> new TableCell<ParkingTicket, MonetaryAmount>() {
            @Override
            protected void updateItem(MonetaryAmount item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setText(null);
                } else {
                    // Format Amount.
                    setText(item.toString() );
                }
            }
        });
       
        /*
         *  Custom rendering for Date and Time Fields for start time column
         */
        startTimeColumn.setCellFactory((TableColumn<ParkingTicket, LocalDateTime> column) -> new TableCell<ParkingTicket, LocalDateTime>() {
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setText(null);
                } else {
                    // Format date and time as per Settings Format .
                    setText(formatter.getFormattedTime( 		
                    				ticketMachineMain.getApplicationSettings().getDateTimeFormatter(),
                    				item ) );
                }
            }
        });
       
        /*
         *  Custom rendering for Date and Time Fields for end time column
         */
        endTimeColumn.setCellFactory((TableColumn<ParkingTicket, LocalDateTime> column) -> new TableCell<ParkingTicket, LocalDateTime>() {
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);
                
                if (item == null || empty) {
                    setText(null);
                    setStyle("");
                } else {
                    // Format date and time as per Settings Format .
                    setText(formatter.getFormattedTime(
            				ticketMachineMain.getApplicationSettings().getDateTimeFormatter(),
            				item ) );
                }
            }
        });
    }    
    
    /**
     * Called when the user clicks on the filter by expire button
     */
    @FXML
    private void handleDownLoadButton () {
        
    }
    
    /**
     * Called when the user clicks on the search button
     */
    @FXML
    private void handleOKButton () {
        dialogStage.close();
    }

    /**
     * Sets the stage of this dialog.
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
}
