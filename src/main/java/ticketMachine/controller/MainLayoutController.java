package ticketMachine.controller;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import ticketMachine.appUtil.Constant;
import ticketMachine.model.ApplicationSettings;
import ticketMachine.model.ParkingTicket;

/** MainLayoutController.java
 *  
 *  Description	: This class belongs to controller part of the parking ticket machine main layout
 *      It is the main Controller for the application.
 * 
 *  @author 	:   Vinayak Hegde
 *  @version 	:	1.0 November 2016
 */
public class MainLayoutController 
        implements Initializable{
    
    private ApplicationSettings settings ;				    /* ApplicationSettings to store and modify the settings	*/
    private ObservableList<ParkingTicket> vehicleList ;		/* The data as an observable list of Vehicles.	*/
    private String adminPassword ;						    /* To authenticate the session for Administration panel */
    
    private Stage primaryStage ;
    private BorderPane mainLayout ;
    
    private SplitPane issueTicketPane ;
    private SplitPane administrationPane ;
    private AnchorPane initSettingsPane ;
    
    private IssueTicketController issueTicketcontroller ;
    private AdministrationController administrationController ;

    /**
     * Default constructor - for Main Layout controller
     */
    public MainLayoutController () {
    	// Do nothing
    }
    
    /**
     * set reference to Primary Stage and Main Layout.
     * @param primaryStage primaryStage to attach user message windows
     * @param mainLayout main layout to switch between different window panes.
     */
    public void setTicketMachineMain (Stage primaryStage, BorderPane mainLayout) {
        this.primaryStage = primaryStage ;
        this.mainLayout = mainLayout ;
        loadInitSettings() ;					    /* Load Initial Settings Pane 	*/
        setMainLayoutCenter(initSettingsPane) ;	    /* Set the main layout center with Settings Pane	*/
    }

	/**
    * Finish a transaction by adding the details into ObservableList
    * @param parkingTicket vehicle object with complete parking ticket details.
    */
    void finishTransaction(ParkingTicket parkingTicket) {
       vehicleList.add(parkingTicket) ;
    }
    
    /**
    * Returns the data as an observable list of Vehicles. 
    * @return vehicleList list of vehicles that have been issed with parking ticket.
    */
    public ObservableList<ParkingTicket> getVehicleList() {
        return this.vehicleList;
    }
    
    /**
     * Getter method for Application Settings.
     * @return settings Application Settings
     */
    public ApplicationSettings getApplicationSettings ()   {
        return this.settings ;
    }
    /**
     * Setter method for Application Settings.
     * @param settings Application Settings
     */
    public void setApplicationSettings ( ApplicationSettings settings)   {
        this.settings = settings ;
    }
    
    /**
     * Getter method for Primary Stage.
     * @return primaryStage primary stage
     */
    public Stage getPrimaryStage() {
        return this.primaryStage    ;
    }
  
    /**
     * Getter method for Admin Password.
     * @return adminPassword Admin Password
     */
    public String getAdminPassword() {
        return this.adminPassword    ;
    }
    /**
     * Setter method for Admin Password
     * @param adminPassword Admin Password
     */
    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword   ;
    }

    /**
     * Set the scene for issue parking ticket window pane
     */
    public void setSceneToIssueTicket() {
        setMainLayoutCenter(issueTicketPane) ;
        issueTicketcontroller.startNewTransaction()   ;	// Start fresh transaction
    }
    
    /**
     * Set the scene for administration window panel
     */
    public void setSceneToAdministration() {
        setMainLayoutCenter(administrationPane) ;
        administrationController.refreshTable() ;
    }

    /**
     * Set the center pane of Main Layout with current scene pane.
     * @param scenePanel Scene Panel
     */
    protected void setMainLayoutCenter(Node scenePanel) {
        mainLayout.setCenter(scenePanel);
    }
    
    /**
     * Load Initialize Settings window pane.
     * Pass the reference to initSettingsController.
     */
    private void loadInitSettings() {
    	try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(Constant.INIT_SETTINGS_FXML));
            initSettingsPane = fxmlLoader.load();
            InitSettingsController initSettingsController = fxmlLoader.getController();
            initSettingsController.setMainLayoutController(this);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
	}

    /**
     * Load Issue Parking Ticket window pane.
     * Pass the reference to issueTicketcontroller.
     */
    public void loadFxmlIssueTicket() {
        try{        
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(Constant.ISSUE_TICKET_FXML));
            issueTicketPane = fxmlLoader.load();
            issueTicketcontroller = fxmlLoader.getController() ;          
            issueTicketcontroller.setMainLayoutController(this);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    /**
     * Load Issue Administration window pane.
     * Pass the reference to administrationController.
     */
    public void loadFxmlAdministration() {
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(Constant.ADMINISTRATION_FXML));
            administrationPane = fxmlLoader.load();
            administrationController = fxmlLoader.getController() ;		  
            administrationController.setMainLayoutController(this); 	
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    /**
     * Show Error Message window.
     * @param title Title for the User message window
     * @param headerText Header for the User message window
     * @param contentText User Message Text
     */
    protected void showErrorMessage(String title, String headerText, String contentText) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.initOwner(primaryStage);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        alert.showAndWait();
    }

    /**
     * Show Confirmation window.
     * @param title Title for the User message window
     * @param headerText Header for the User message window
     * @param contentText User Message Text
     */
    protected void showConfirmationMessage(String title, String headerText, String contentText) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.initOwner(primaryStage);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        alert.showAndWait();
    }

    /**
     * Default initialization for FXML controller
     */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}

    public void createSettings(String[] localeCodes, String dateTimeFormat) {
        if (localeCodes.length==2) {
            Locale.setDefault(new Locale(localeCodes[0], localeCodes[1]) );
        }
        settings = new ApplicationSettings () ;
        settings.setDateTimeFormat(dateTimeFormat);
        vehicleList = FXCollections.observableArrayList();
    }
}
